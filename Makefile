SRCDIR := .

include Targets

all: $(TARGETS)

$(TARGETS): %.inform
	$(eval PROJDIR := $(SRCDIR)/$@.inform)
	$(eval SRCS := $(wildcard $(PROJDIR)/Source/*.ni))
	$(eval I6S := $(patsubst %.ni,%.i6,$(SRCS)))
	$(INFORM7) -project "$(PROJDIR)" $(ARGS)
	$(INFORM6) -E2w~S~DG $(PROJDIR)/Build/auto.inf $(PROJDIR)/Build/output.ulx
	$(INBLORB) $(PROJDIR)/Release.blurb $(SRCDIR)/$@.gblorb
	$(IF)/web.sh $@

%.inform:
	echo $@

clean_inform:
	echo $(SRCDIR)/$(TARGET_NAME)/Build

web:
	$(foreach TARGET_NAME, $(TARGETS), - python3 $(IF)/blorbtool.py $(TARGET_NAME).gblorb giload $(TARGET_NAME).materials/Release/interpreter interpreter && echo `pwd` && cp $(TARGET_NAME).gblorb "$(TARGET_NAME).materials/Release/`cat $(TARGET_NAME).inform/Release.blurb | grep -Eo -m1 '([^\"]+\.gblorb)' | sed -e 's/\( \)/\\ /g'`" )

clean:
	$(foreach TARGET_NAME, $(TARGETS), - rm -rf $(SRCDIR)/$(TARGET_NAME).inform/Build - rm -rf $(SRCDIR)/$(TARGET_NAME).materials/Release $(SRCDIR)/$(TARGET_NAME).inform/Index $(SRCDIR)/$(TARGET_NAME).inform/*.plist $(SRCDIR)/$(TARGET_NAME).inform/*.iFiction $(SRCDIR)/$(TARGET_NAME).inform/*.blurb )
	- rm *.gblorb
