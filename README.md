# What to do with this?
Use it as a template for projects when you don't want to use IDE's (I'm looking at you code warriors).  Keep in mind, this is an opinionated approach to the entire process of Interactive Fictions using the Inform 7 toolchain by Graham Nelson.

## The Environment
You'll need to define the following environment variables somehow (either globally or local to user or whatever).

```
IF # root of where you have all the interactive fiction tools stuff
INFORM # Inform toolchain's home path
INFORM7 # The path to the executable for inform7
INFORM6 # The path to the executable for inform6
INBLORB # The path to the executable for inblorb
```

## Tools Installed
* Python 3 (I'm being opinionated here!)
* [Graham Nelson's Inform 7 toolchain](https://github.com/ganelson/inform)
* [blorbtool.py](https://github.com/erkyrath/glk-dev/blob/master/blorbtool.py)
* web.sh (included here and needs to be in the IF path as specified)

## For VSCode users unwilling to make global settings changes
I have included the settings.json with necessary changes for allowing VSCode to use tab indention but only for Inform 7 and can be included in your global settings.