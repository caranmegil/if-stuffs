#!/bin/sh

python3 ${IF}/blorbtool.py $1.gblorb giload $1.materials/Release/interpreter interpreter
cp $1.gblorb "$1.materials/Release/`cat $1.inform/Release.blurb | grep -Eo -m1 '([^\"]+\.gblorb)' | sed -e 's/\( \)/\\ /g'`"
